<?php
/** Implements hook_views_data_alter()
 */
function views3_entity_field_views_data_alter(&$data){
	# List all entities & their info
	$views_map = array(); # array( entity_name => entity_info )
	foreach (field_info_bundles() as $entity_name => $bundles) # $bundles: array( bundle_name => bundle_info )
		$views_map[$entity_name] = entity_get_info($entity_name);

	# Alter all entity fields
	foreach ($views_map as $entity_name => $entity_info)
		$data[  $entity_info['base table']  ]['entity_field'] = array(
			'field' => array(
				'title' => t('Entity @entity field', array('@entity' => $entity_info['label'])),
				'help' => t('Render the entity in a specified view mode.'),
				'handler' => 'views3_entity_field_entity_field',
			),
		);
}