<?php
class views3_entity_field_entity_field extends views_handler_field {
	/** Entity name
	 * @var string
	 */
	public $entity_name;

	/** Entity info array
	 * @var array
	 */
	public $entity_info;

	/** Entity 'id' column name.
	 * Example: for 'node' that's 'nid'
	 * @var string
	 */
	public $entity_info_id;

	/** List all entities' info & find Entity Name by Table Name
	 */
	protected function _get_entity_name(){
		# Unfortunately, it's impossible to pass custom params to the handler.

		# List all entities.... :(
		foreach (field_info_bundles() as $entity_name => $bundles){
			$entity_info = entity_get_info($entity_name);
			if ($entity_info['base table'] == $this->table)
				return $entity_name;
		}

		# Fallback
		return $this->table;
	}

	function init(&$view, $options) {
		parent::init($view, $options);
		$this->entity_name = $this->_get_entity_name();
		$this->entity_info = entity_get_info($this->entity_name);
		$this->entity_info_id = $this->entity_info['entity keys']['id'];
		# tell Views to load additional fields.
		$this->additional_fields[$this->entity_info_id] = $this->entity_info_id; # ID of the entity. Ex: for 'node' that's 'nid'
		}

	function option_definition() {
		$options = parent::option_definition();
		$options['view_mode'] = array('default' => '');
		$options['links'] = array('default' => FALSE);
		$options['comments'] = array('default' => FALSE);
		return $options;
	}

	function options_form(&$form, &$form_state) {
		parent::options_form($form, $form_state);

		$form['view_mode'] = array(
			'#type' => 'textfield',
			'#title' => t('View Mode'),
			'#description' => t(''),
			'#default_value' => $this->options['view_mode'],
		);

		$form['links'] = array(
			'#type' => 'checkbox',
			'#title' => t('Display links'),
			'#default_value' => $this->options['links'],
		);
		$form['comments'] = array(
			'#type' => 'checkbox',
			'#title' => t('Display comments'),
			'#default_value' => $this->options['comments'],
		);
	}

	function query(){
		$this->ensure_my_table();
		$this->add_additional_fields(); # Additional fields to add to the query (definde in the constructor)
	}

	function render($values){
		# Get the entity ID
		$entity_id = $values->{$this->aliases[$this->entity_info_id]};
		if (!$entity_id)
			return array();

		# Load the entity
		$entity = entity_load_single($this->entity_name, $entity_id);
		if (!$entity)
			return array();

		# Render
		$entity->view = $this->view;
		$content = entity_view($this->table, array($entity_id => $entity), $this->options['view_mode']);

		# Additional options
		if (!$this->options['links'])
			unset($content['links']);
		if ($this->options['comments'] && user_access('access comments') && $entity->comment)
			$content['comments'] = comment_node_page_additions($entity);

		# Hooks
		$view_display_name = "{$this->view->name}.{$this->view->current_display}";
		module_invoke_all('views3_entity_field_render', $view_display_name, $this->view, $this, $values, $entity_id, $entity, $content);

		# Finish
		return $content;
	}
}